from collections import defaultdict, Counter
import sys

def do_match(seq1, seq2, mismatch_threshold):
    ## Returns whether two strings are identical based on an allowed proportion
    ## of mismatches (mismatch_threshold); this is same as dupe_thresh later
    min_len = min(len(seq1), len(seq2))
    max_len = min(len(seq1), len(seq2))

    # Maximum number of mismatches that can occur to meet the mismatch threshold
    allowed_fails = max_len*(1-mismatch_threshold)

    fails = 0
    matches = 0
    for i in range(min_len):
        c1 = seq1[i]
        c2 = seq2[i]
        does_match = c1==c2
        matches += does_match
        fails += (not does_match)
        if fails > allowed_fails: # Can't meet match threshold
            return False

    return matches/max_len >= mismatch_threshold


def extract_dicts(file1_name, file2_name, no_lines):
    ## Returns a dictionary {left_seq,right_seq: locations in the fastq file]}
    with open(file1_name) as file1, open(file2_name) as file2:

        ## Parse the .fastq files
        left_seqs = []
        for i, line in enumerate(file1):
            if i >= no_lines:
                break
            if i%4 == 1:
                left_seqs.append(line[:-1])

        right_seqs = []
        for i, line in enumerate(file2):
            if i >= no_lines:
                break
            if i%4 == 1:
                right_seqs.append(line[:-1])

    # Define a full fragment by the concatenation of the left and right reads
    seqs = [left_seqs[i] + "," + right_seqs[i] for i in range(
            len(left_seqs))]

    seqs_dict = defaultdict(list)
    for i, seq in enumerate(seqs):
        seqs_dict[seq].append(i)

    return seqs_dict


def find_dupes(seqs, dupe_threshold = 1):
    # Main inputs a dict {"seq", [i]} into this
    # Returns a set of integers representing the locations of reads to be removed
    removals = set()
    if len(seqs) > 1:
        unique_seqs = set(i[0] for i in seqs)

        unique_seqs_dict = seqs
        possible_removals = sorted(unique_seqs_dict.items(),
                                  key=lambda i: len(i[1]),
                                   reverse=True) # generates a list of tuples (seq, locs) sorted by descending len(locs)
        for i, seqlocs in enumerate(possible_removals):
            seq, locs = seqlocs
            for j in locs[1:]:
                removals.add(j)

##            # Find partial matches
##            if dupe_threshold < 1:
##                for seq_to_compare_to, k in possible_removals[:i]:
##                    if k[0] not in removals and do_match(
##                                seq_to_compare_to, seq, dupe_threshold):
##                        removals.add(locs[0])
##                        break

    return removals


def write_file_wo_dupes(file1_name, file2_name, dupes, no_seqs):
    ## Write two new files (left and right read) without the duplicates
    if not dupes: dupes = [None]
    else: dupes = sorted(dupes)

    # Left reads
    with open(file1_name) as read1:
        j = 0
        non_dupes = [] # Will write this to the no dupes file
        for i, line in enumerate(read1):
            if i >= no_seqs*4:
                break
            if i//4 != dupes[j]:
                # Only write to the no dupes file if the sequence is not in dupes
                non_dupes.append(line)
            elif i%4 == 3 and j != len(dupes)-1:
                j += 1

    file_name_split = file1_name.split(".")
    with open(".".join(file_name_split[:-1])+"_no_dupes_no_umis."+
              file_name_split[-1], "w", newline="\n") as write1:
        write1.write("".join(non_dupes))

    # Right reads
    with open(file2_name) as read2:
        j = 0
        non_dupes = []
        for i, line in enumerate(read2):
            if i >= no_seqs*4:
                break
            if i//4 != dupes[j]:
                non_dupes.append(line)
            elif i%4 == 3 and j != len(dupes)-1:
                j += 1

    file_name_split = file2_name.split(".")
    with open(".".join(file_name_split[:-1])+"_no_dupes_no_umis."+
              file_name_split[-1], "w", newline="\n") as write2:
        write2.write("".join(non_dupes))
    

def main(file1_name, file2_name, dupe_thresh=1,
         first=float("inf")):
    print("script initiated")
    seqs = extract_dicts(file1_name, file2_name, first*4)
    print("sequences extracted")
##    hops = 0
##    total = 0
##    # Reassign the index hoppers
##    for seq, umis_with_seq in seqs.items():
##        counts = list(Counter(umis_with_seq).items())
##        true_umi = counts[0][0] # UMI with appears with the highest frequency
##                                # is deemed to be the correct one
##        hopped_umis = [i[0] for i in counts[1:]]
##
##        for hopped_umi in hopped_umis:
##            for seqloc in umis[hopped_umi]:
##                if seqloc[0] == seq:
##                    # Remove each sequence from the hopped UMI, and add to the true UMI
##                    umis[hopped_umi].remove(seqloc)
##                    umis[true_umi].append(seqloc)
##        
##        hops += max(i[1] for i in counts)
##        total += sum(i[1] for i in counts)
##
##    hop_percent = str(round((1-hops/total)*100, 1))
##    #print("Index hoppers reassigned: "+str(total-hops)+
##    #      " (" + hop_percent + "%)")
##    print(total-hops,end="\t")

    with open(file1_name) as file:
        no_seqs = sum(1 for line in file)//4
    all_dupes = []
    dupes = find_dupes(seqs, dupe_thresh)
    for d in dupes:
        all_dupes.append(d)

    write_file_wo_dupes(file1_name, file2_name, all_dupes, first)

    dupe_percent = str(round(len(all_dupes)/no_seqs*100, 1))
    print("Duplicates removed: "+str(len(all_dupes)))
    #print(len(all_dupes),end="\t")


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
